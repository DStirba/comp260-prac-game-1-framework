﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    //Publics
    public float minSpeed;
    public float maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    public ParticleSystem explosionPrefab;
    public Transform[] target;
    public Vector2 V;
    public Vector2 vel;

    //Privates
    private float dt;
    private Vector2 heading = Vector2.right;
    private float speed = 4.0f;
    private float turnSpeed;

    private void Start()
    {
        //find the players in the scene.
        PlayerMove[] player = FindObjectsOfType<PlayerMove>();
        for (int i = 0; i< target.Length; i++)
        {
            target[i] = player[i].transform;
        }

        //setting inital angles for the bee to turn too
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        //setting the Turnspeed and speed to a random value.
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);

    }

    // gets the Closest index from the Bee to the Players
    int get_index_from_Magnitude(Transform[] arr)
    {
        int index = 0;

        for (int i = 0; i < arr.Length - 1; i++)
        {
            //is the magnitude closer to the bee?
            if (arr[i].position.magnitude < arr[i + 1].position.magnitude)
            {
                index = i;
            } else
            {
                index = i + 1;
            }
        }

        return index;
    }

	// Update is called once per frame
	void Update () {

       dt = Time.deltaTime;

        int ClosestIndex = get_index_from_Magnitude(target);

        Debug.Log("Closest Player is:" + target[ClosestIndex].name + "With a Magnitude of:" + target[ClosestIndex].position.magnitude.ToString());

        V = target[ClosestIndex].position - transform.position;

        float angle = turnSpeed * dt;

        if (V.IsOnLeft(heading))
        {
            heading = heading.Rotate(angle);
        }
        else
        {
            heading = heading.Rotate(-angle);
        }

        V = V.normalized;
        vel = V * speed;

        transform.Translate(vel * speed * dt);

        speed = Mathf.Clamp(speed,minSpeed,maxSpeed);

	}

    void OnDestroy()
    {
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position, V);
    }
}
