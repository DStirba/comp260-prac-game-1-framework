﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public int nBees = 50;
    public BeeMove beePreFab;
    private PlayerMove player;
    public Rect spawnRect;
    public float minBeePeroid = 2;
    public float maxBeePeroid = 5;
    public float beePeroid;
    private float time;
    private int count = 0;
    // Use this for initialization
    void Start () {
        beePeroid = Random.Range(minBeePeroid, maxBeePeroid);
        time = 0;
        BeeMove bee = new BeeMove();
        // instantiate the bees
        for (int i = 0;  i < nBees; i++)
        {
            bee = Instantiate(beePreFab);

            bee.transform.parent = transform;

            bee.gameObject.name = "Bee" + i;

            float x = spawnRect.xMin + Random.value * spawnRect.width;
            float y = spawnRect.yMin + Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x,y);
        }

        //set the target
        for (int i = 0; i < bee.target.Length; i++)
        {
            bee.target[i] = player.transform;
        }

    }


    void OnDrawGizmos()
    {   // draw the spawning rectangle   
        Gizmos.color = Color.green;
        Gizmos.DrawLine(                 
            new Vector2(spawnRect.xMin, spawnRect.yMin),                  
            new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(                 
            new Vector2(spawnRect.xMax, spawnRect.yMin),                  
            new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(                 
            new Vector2(spawnRect.xMax, spawnRect.yMax),                  
            new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(                 
            new Vector2(spawnRect.xMin, spawnRect.yMax),                  
            new Vector2(spawnRect.xMin, spawnRect.yMin));
    } 

    // Update is called once per frame
    void FixedUpdate () {
        time += Time.deltaTime;
        if (time >= beePeroid)
        {
            
            BeeMove bee = new BeeMove();


            time = 0;
            bee = Instantiate(beePreFab);

            bee.transform.parent = transform;

            bee.gameObject.name = "Bee Random Spawn " + count;

            float x = spawnRect.xMin + Random.value * spawnRect.width;
            float y = spawnRect.yMin + Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);

            count++;

            beePeroid = Random.Range(minBeePeroid, maxBeePeroid);

        }
	}

    public void DestroyBees(Vector2 centre, float radius)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }


}
